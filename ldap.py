from nethz.ldap import AuthenticatedLdap
import ldap3
from pprint import pprint
from config import LDAP_PASS, LDAP_USER

search_base = 'ou=nethz,ou=id,ou=auth,o=ethz,c=ch'

search_filter = 'althamm'

connector = AuthenticatedLdap(LDAP_USER, LDAP_PASS)

def test():
  query = "(cn=%s)" % _escape(search_filter)
  ldap_data = next(_search(query), None)

def _search(query):
  """Search the LDAP. Returns filtered data (iterable) for string query."""
  attr = [
      '*',
  ]
  results = connector.search(query, attributes=attr)
  for res in results:
    print(type(res))
    pprint(dict(res), width=1)
  return (_process_data(res) for res in results)

def _escape(query):
  """LDAP-style escape according to the ldap3 documentation."""
  replacements = (
    ('\\', r'\5C'),  # Do this first or we'll break the other replacements
    ('*', r'\2A'),
    ('(', r'\28'),
    (')', r'\29'),
    (chr(0), r'\00')
  )
  for old, new in replacements:
    query = query.replace(old, new)

  return query

test()
